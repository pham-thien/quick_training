$(function(){

	// 整備士選択アコーディオン
	var mechanic_acd_action = function() {
		// slideUp, slideDownだとガタつく
		$(document).ready(function() {
			var tar_trg     = $('.experience').find('select[name="career1_expocupctgid"]'),
				tar_area    = tar_trg.parents('tr').next('tr').find('th > .acdArea, td > .acdArea'),
				parent_tr   = tar_area.parents('tr'),
				parent_tr_h = parent_tr.height(),
				parent_icon = parent_tr.find('.must, .any'),
				other_check = tar_area.find('input[type=checkbox].acdTrg'),
				other_txt_h = tar_area.find('.acdArea').height(),
				tar_val     = '14',
				duration    = 400,
				anm_flg;
			// 「メカニック・整備士」以外が選択されているとき選択範囲を閉じる
			if(tar_trg.val() === tar_val){
				parent_tr.css('border-bottom', '1px solid #E6E6E6');
				tar_area.css({
					paddingTop    : 20,
					paddingBottom : 20,
					height        : 'auto',
				});
			} else {
				tar_area.add(parent_icon).hide();
				tar_area.css({
					paddingTop    : 0,
					paddingBottom : 0,
					height        : 0,
					opacity       : 0,
				});
			}
			tar_trg.change(function() {
				var this_val = $(this).val();
				if(anm_flg === true) { return; }
				anm_flg = true;
				if(this_val === tar_val) {
					tar_area.show().animate({
						paddingTop    : 20,
						paddingBottom : 20,
						height        : parent_tr_h,
					}, duration, function() {
						tar_area.each(function() {
							var this_area = $(this);
							if(this_area.parent().is('th')) {
								this_area.css({
									height     : 'auto',
									display    : 'flex',
									alignItems : 'center',
									opacity    : 1,
								});
							} else if(this_area.parent().is('td')) {
								this_area.css({
									height  : 'auto',
									opacity : 1,
								});
							};
						})
						parent_tr.css('border-bottom', '1px solid #E6E6E6');
						parent_icon.fadeIn(duration*0.5, function() {
							anm_flg = false;
						});
					});
				} else {
					parent_icon.fadeOut(duration*0.5);
					tar_area.animate({
						paddingTop    : 0,
						paddingBottom : 0,
						height        : 0,
						opacity       : 0,
					}, duration, function() {
						tar_area.hide();
						parent_tr.css('border-bottom', 'none');
						anm_flg = false;
					});
				}
			});
			other_check.click(function() {
				var this_prop = $(this).prop('checked');
				if(this_prop === true) {
					parent_tr_h += other_txt_h;
				} else if(this_prop === false) {
					parent_tr_h -= other_txt_h;
				}
			});
		});
	}();

	// 資格アコーディオン
	var license_acd_action = function() {
		var tar_trg  = $('input[type=checkbox].acdTrg'),
			duration = 200,
			tar_val  = '6',
			anm_flg;
		tar_trg.each(function() {
			var this_trg = $(this),
				this_are = this_trg.parents('.checkList').next('.acdArea');
			if(this_trg.prop('checked') === true) {
				this_are.show();
			} else if(this_trg.prop('checked') === false) {
				this_are.hide();
			}
		});
		tar_trg.click(function() {
			var this_trg = $(this),
				trg_prop = this_trg.prop('checked'),
				area     = this_trg.parents('.checkList').next('.acdArea');
			if(anm_flg === true) { return; }
			anm_flg = true;
			if(trg_prop === true) {
				area.slideDown(duration, function() {
					anm_flg = false;
				});
			} else if(trg_prop === false) {
				area.slideUp(duration, function() {
					anm_flg = false;
				});
			}
		});
	}();

	// submit後に完了ボタンの押下を不可にして再押下させない
	$('#form').submit(function(){
		$('.submitBtn .button input[name="_submit"]').prop('disabled', true);
		return true;
	});
	
});