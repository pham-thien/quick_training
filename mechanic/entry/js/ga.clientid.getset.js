	function loopSleep(loopLimit, interval, mainFunc) {
		var i = 0;
		var loopFunc = function() {
			var result = mainFunc(i);
			if (result === false) {
				return;
			}
			i = i + 1;

			if (i < loopLimit) {
				setTimeout(loopFunc, interval);
			}
		};
		loopFunc();
	}

	loopSleep(10, 2000, function(i) {
		if (typeof(ga) !== 'undefined') {
			var clientid = null;
			ga(function(tracker) {
				var trackers = ga.getAll();
				trackers.forEach(function(tracker) {
					clientid = tracker.get('clientId');
				});
			});
			if(clientid == null || document.getElementById('ga_client_id') == null){
				return true;
			}else{
				document.getElementById('ga_client_id').value = clientid;
				return false;
			}
		}
	});
