<?php 
function update($filepath) {
	echo date('YmdHis', filemtime($_SERVER['DOCUMENT_ROOT'].$filepath));
}
?>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=no">
    <meta name="format-detection" content="telephone=no">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <title>自動車業界専門の転職エージェント - オートモーティブ・ジョブズ</title>
    <meta name="robots" content="noindex">
    <link rel="icon" href="/img/common/favicon.ico" type="image/x-icon">
    <link rel="Shortcut Icon" type="image/x-icon" href="/img/common/favicon.ico">
    <!-- Start Visual Website Optimizer Synchronous Code -->
    <script>
    var _vis_opt_account_id = 279593;
    var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
    document.write('<s' + 'cript src="' + _vis_opt_protocol +
        'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a=' + _vis_opt_account_id + '&url=' +
        encodeURIComponent(document.URL) + '&random=' + Math.random() + '">' + '<\/s' + 'cript>');
    </script>

    <script>
    if (typeof(_vis_opt_settings_loaded) == "boolean") {
        document.write('<s' + 'cript src="' + _vis_opt_protocol +
            'd5phz18u4wuww.cloudfront.net/vis_opt.js">' + '<\/s' + 'cript>');
    }
    /* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
    </script>

    <script>
    if (typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize();
        vwo_$(document).ready(function() {
            _vis_opt_bottom_initialize();
        });
    }
    </script>
    <!-- End Visual Website Optimizer Synchronous Code -->
    <link href="/mechanic/lp/s/css/common.css?<?php update('/mechanic/lp/s/css/common.css'); ?>" rel="stylesheet">
    <link href="/mechanic/s/css/form.css?<?php update('/mechanic/s/css/form.css'); ?>" rel="stylesheet">
    <link href="/mechanic/lp/s/aj00006/css/index.css?<?php update('/mechanic/lp/s/aj00006/css/index.css'); ?>" rel="stylesheet">
    <script src="/lp/s/js/common.js?<?php update('/lp/s/js/common.js'); ?>"></script>
</head>

<body id="aj00006">
    <!-- Google Tag Manager -->
    <noscript><iframe src="//www.googletagmanager.com/ns.html?id=GTM-TB4VPP" height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src =
            '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-TB4VPP');
    </script>
    <!-- End Google Tag Manager -->
    <div id="container">
        <header id="header">
            <div class="inner">
                <h1 class="logo"><img src="/mechanic/lp/s/aj00006/img/logo.png" alt="整備士JOBS ご友人優待キャンペーン"></h1>
            </div>
        </header>
        <main id="main">
            <div id="mainVisual">
                <p class="catch"><img src="/mechanic/lp/s/aj00006/img/lp_img01.png" alt="新規で転職サポートに申し込むとギフト券5,000円プレゼント 高給与・好待遇の求人豊富 整備士ジョブズ"></p>
                <div class="anchBn">
                    <a href="#formBox"><img src="/mechanic/lp/s/aj00006/img/lp_bn01.png" width="475" height="81" alt="転職サポートを依頼する"></a>
                </div>
            </div>
            <div id="conts">
                <div class="targetWrapper">
                    <div class="target">
                        <p class="targetHead"><img src="/mechanic/lp/s/aj00006/img/lp_ttl.png" width="96" height="21" alt="対象条件"></p>
                        <div class="targetInner">
                            <dl class="targetBlock">
                                <dt class="targetTtl">職種</dt>
                                <dd class="targetTxtBlock">
                                    <ul class="targetTxt">
                                        <li>自動車整備士</li>
                                    </ul>
                                </dd>
                            </dl>
                            <dl class="targetBlock">
                                <dt class="targetTtl">資格</dt>
                                <dd class="targetTxtBlock">
                                    <ul class="targetTxt">
                                        <li>三級自動車整備士</li>
                                        <li>二級自動車整備士</li>
                                        <li>一級自動車整備士<span>のいずれか</span></li>
                                    </ul>
                                </dd>
                            </dl>
                        </div>
                        <dl class="attention">
                            <dt><span>注意事項</span></dt>
                            <dd>弊社でのご支援が難しい場合は、大変恐縮ながらキャンペーン対象外とさせて頂く可能性がございます。</dd>
                        </dl>
                    </div>
                </div>
                <div class="section01">
                    <p class="logo"><img src="/mechanic/img/common/logo.svg" width="150" alt="整備士JOBS"></p>
                    <p class="section01Ttl"><img src="/mechanic/lp/s/aj00006/img/lp_ttl03.png" width="284" alt="自動車整備士の転職・求人サイト。"></p>
                    <p class="section01Txt">高給与・好待遇の求人が豊富。<br>
                        年収交渉・待遇交渉で条件UP。</p>
                </div>
                <div class="section02">
                    <p class="section02Ttl">今すぐ転職の予定がない場合も、<br>
                        お気軽にお申し込みください。</p>
                    <p class="section02Txt">将来の転職を有利にする情報や<br>各種アドバイスを提供いたします。</p>
                </div>
                <div class="formBox" id="formBox">
                    <div class="formTtl01">
                        <p>無料転職サポート お申込フォーム</p>
                    </div>
                    <div class="please">
                        <p>必要事項を入力のうえ、「申し込む」ボタンをタップしてください</p>
                    </div>
                    <script src="/mechanic/lp/s/js/multi_step_form.js"></script>
                    <script src="/mechanic/entry/js/ga.clientid.getset.js"></script>
                    <form method="post" name="entry_form">
                        <input type="hidden" name="_exec" value="regist">
                        <input type="hidden" name="_form_mode" value="mechanic">
                        <input type="hidden" name="ga_client_id" id="ga_client_id">
                        <input type="hidden" name="_media_identification" value="introduction_lp">
                        <div class="step">
                            <div class="meter">
                                <div class="meter_inner"></div>
                            </div>
                            <div class="txt"><span>1</span> / 3</div>
                        </div>

                        <div class="layerWrapper">
                            <!-- STEP01 -->
                            <div class="layer">
                                <div class="inputArea">
                                    <dl>
                                        <dt>お住まい<span class="errorTxt">【注】未選択です</span></dt>
                                        <dd>
                                            <div class="selectArea">
                                                <p data-panelname="address" class="slideTrg">選択してください</p>
                                            </div>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>お持ちの資格<span class="errorTxt">【注】未選択です</span></dt>
                                        <dd>
                                            <div class="selectArea">
                                                <p data-panelname="license" class="slideTrg">選択してください</p>
                                            </div>
                                        </dd>
                                    </dl>
                                </div>
                                <div class="btnField">
                                    <button type="button" disabled="disabled" class="trnsBtn subBtn">次へ</button>
                                </div>
                            </div>
                            <!-- STEP02 -->
                            <div class="layer">
                                <div class="inputArea">
                                    <dl>
                                        <dt>生年月日<span class="errorTxt">【注】未選択です</span></dt>
                                        <dd class="birth">
                                            <div class="selectArea">
                                                <select name="_birth_y" data-group="birthday" class="validGroup">
                                                    <option value="">---</option>
                                                    <option value="1999">1999</option>
                                                    <option value="1998">1998</option>
                                                    <option value="1997">1997</option>
                                                    <option value="1996">1996</option>
                                                    <option value="1995">1995</option>
                                                    <option value="1994">1994</option>
                                                    <option value="1993">1993</option>
                                                    <option value="1992">1992</option>
                                                    <option value="1991">1991</option>
                                                    <option value="1990">1990</option>
                                                    <option value="1989">1989</option>
                                                    <option value="1988">1988</option>
                                                    <option value="1987">1987</option>
                                                    <option value="1986">1986</option>
                                                    <option value="1985">1985</option>
                                                    <option value="1984">1984</option>
                                                    <option value="1983">1983</option>
                                                    <option value="1982">1982</option>
                                                    <option value="1981">1981</option>
                                                    <option value="1980">1980</option>
                                                    <option value="1979">1979</option>
                                                    <option value="1978">1978</option>
                                                    <option value="1977">1977</option>
                                                    <option value="1976">1976</option>
                                                    <option value="1975">1975</option>
                                                    <option value="1974">1974</option>
                                                    <option value="1973">1973</option>
                                                    <option value="1972">1972</option>
                                                    <option value="1971">1971</option>
                                                    <option value="1970">1970</option>
                                                    <option value="1969">1969</option>
                                                    <option value="1968">1968</option>
                                                    <option value="1967">1967</option>
                                                    <option value="1966">1966</option>
                                                    <option value="1965">1965</option>
                                                    <option value="1964">1964</option>
                                                    <option value="1963">1963</option>
                                                    <option value="1962">1962</option>
                                                    <option value="1961">1961</option>
                                                    <option value="1960">1960</option>
                                                    <option value="1959">1959</option>
                                                    <option value="1958">1958</option>
                                                    <option value="1957">1957</option>
                                                    <option value="1956">1956</option>
                                                    <option value="1955">1955</option>
                                                    <option value="1954">1954</option>
                                                    <option value="1953">1953</option>
                                                </select>
                                            </div>
                                            <span class="textSpan02">年</span>
                                            <div class="selectArea">
                                                <select name="_birth_m" data-group="birthday" class="validGroup">
                                                    <option value="">---</option>
                                                    <option label="1" value="1">1</option>
                                                    <option label="2" value="2">2</option>
                                                    <option label="3" value="3">3</option>
                                                    <option label="4" value="4">4</option>
                                                    <option label="5" value="5">5</option>
                                                    <option label="6" value="6">6</option>
                                                    <option label="7" value="7">7</option>
                                                    <option label="8" value="8">8</option>
                                                    <option label="9" value="9">9</option>
                                                    <option label="10" value="10">10</option>
                                                    <option label="11" value="11">11</option>
                                                    <option label="12" value="12">12</option>
                                                </select>
                                            </div>
                                            <span class="textSpan02">月</span>
                                            <div class="selectArea">
                                                <select name="_birth_d" data-group="birthday" class="validGroup">
                                                    <option value="">---</option>
                                                    <option label="1" value="1">1</option>
                                                    <option label="2" value="2">2</option>
                                                    <option label="3" value="3">3</option>
                                                    <option label="4" value="4">4</option>
                                                    <option label="5" value="5">5</option>
                                                    <option label="6" value="6">6</option>
                                                    <option label="7" value="7">7</option>
                                                    <option label="8" value="8">8</option>
                                                    <option label="9" value="9">9</option>
                                                    <option label="10" value="10">10</option>
                                                    <option label="11" value="11">11</option>
                                                    <option label="12" value="12">12</option>
                                                    <option label="13" value="13">13</option>
                                                    <option label="14" value="14">14</option>
                                                    <option label="15" value="15">15</option>
                                                    <option label="16" value="16">16</option>
                                                    <option label="17" value="17">17</option>
                                                    <option label="18" value="18">18</option>
                                                    <option label="19" value="19">19</option>
                                                    <option label="20" value="20">20</option>
                                                    <option label="21" value="21">21</option>
                                                    <option label="22" value="22">22</option>
                                                    <option label="23" value="23">23</option>
                                                    <option label="24" value="24">24</option>
                                                    <option label="25" value="25">25</option>
                                                    <option label="26" value="26">26</option>
                                                    <option label="27" value="27">27</option>
                                                    <option label="28" value="28">28</option>
                                                    <option label="29" value="29">29</option>
                                                    <option label="30" value="30">30</option>
                                                    <option label="31" value="31">31</option>
                                                </select>
                                            </div>
                                            <span class="textSpan02">日</span>
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>お名前<span class="errorTxt">【注】未入力です</span></dt>
                                        <dd class="name">
                                            <input type="text" name="surname" value="" placeholder="姓" data-group="name" class="blurElm validGroup">
                                            <input type="text" name="firstname" id="fNameMei" value="" placeholder="名" data-group="name" class="blurElm validGroup">
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>ふりがな<span class="errorTxt">【注】未入力です</span></dt>
                                        <dd class="ruby">
                                            <input type="text" name="surname_ruby" value="" placeholder="せい" data-group="ruby" class="blurElm validGroup">
                                            <input type="text" name="firstname_ruby" value="" placeholder="めい" data-group="ruby" class="blurElm validGroup">
                                        </dd>
                                    </dl>
                                </div>
                                <div class="btnField">
                                    <button type="button" class="trnsBtn backBtn">戻る</button>
                                    <button type="button" disabled="disabled" class="trnsBtn subBtn">次へ</button>
                                </div>
                            </div>
                            <!-- STEP03 -->
                            <div class="layer">
                                <div class="inputArea">
                                    <dl>
                                        <dt>メールアドレス<span class="errorTxt">【注】未入力です</span></dt>
                                        <dd class="email">
                                            <input id="fMail" type="email" name="email" value="" placeholder="xxx@yyy.zzz" autocapitalize="off" autocorrect="off" class="blurElm">
                                        </dd>
                                    </dl>
                                    <dl>
                                        <dt>携帯電話番号<span class="errorTxt">【注】未入力です</span></dt>
                                        <dd class="tel">
                                            <input id="fTel" type="tel" name="tel2" value="" placeholder="09012345678" class="blurElm">
                                        </dd>
                                    </dl>
                                </div>
                                <div class="agree">
                                    <div class="arrow"></div>
                                    <div class="policy"><a href="/mechanic/ruleprivacy/" target="_blank">利用規約</a>に同意して</div>
                                </div>
                                <div class="btnField">
                                    <button type="button" class="trnsBtn backBtn">戻る</button>
                                    <button type="submit" disabled="disabled" class="trnsBtn subBtn"><span>送信</span></button>
                                </div>
                            </div>
                        </div>

                        <!-- slidePanel -->
                        <div id="address" class="slidePanel">
                            <div class="slidePanelInner">
                                <div class="topBox">
                                    <span class="txt">お住まいを選択してください</span>
                                    <span class="clsBtn"></span>
                                </div>
                                <ul class="radioBtn">
                                    <li><label><input type="radio" name="m_pref_id" value="1">北海道</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="2">青森県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="3">岩手県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="4">宮城県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="5">秋田県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="6">山形県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="7">福島県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="8">茨城県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="9">栃木県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="10">群馬県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="11">埼玉県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="12">千葉県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="13">東京都</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="14">神奈川県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="15">新潟県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="16">富山県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="17">石川県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="18">福井県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="19">山梨県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="20">長野県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="21">岐阜県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="22">静岡県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="23">愛知県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="24">三重県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="25">滋賀県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="26">京都府</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="27">大阪府</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="28">兵庫県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="29">奈良県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="30">和歌山県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="31">鳥取県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="32">島根県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="33">岡山県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="34">広島県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="35">山口県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="36">徳島県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="37">香川県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="38">愛媛県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="39">高知県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="40">福岡県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="41">佐賀県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="42">長崎県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="43">熊本県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="44">大分県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="45">宮崎県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="46">鹿児島県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="47">沖縄県</label></li>
                                    <li><label><input type="radio" name="m_pref_id" value="48">海外</label></li>
                                </ul>
                                <div class="selectBtn">
                                    <button type="button" disabled="disabled">決　定</button>
                                </div>
                            </div>
                        </div>
                        <div id="license" class="slidePanel">
                            <div class="slidePanelInner">
                                <div class="topBox">
                                    <span class="txt">お持ちの資格を選択してください</span>
                                    <span class="clsBtn"></span>
                                </div>
                                <ul class="checkbox">
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="5"><span class="txt">資格なし</span>
                                        </label></li>
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="1"><span class="txt">三級自動車整備士</span>
                                        </label></li>
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="2"><span class="txt">二級自動車整備士</span>
                                        </label></li>
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="3"><span class="txt">一級自動車整備士</span>
                                        </label></li>
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="4"><span class="txt">自動車検査員</span>
                                        </label></li>
                                    <li><label>
                                            <input type="checkbox" name="_license_id[]" value="6" class="other"><span class="txt">その他</span>
                                        </label></li>
                                    <li class="license_other"><label>
                                            <textarea name="_sp_other_license" placeholder="その他の資格をお持ちの方はご入力ください" disabled="true"></textarea>
                                        </label></li>
                                </ul>
                                <div class="selectBtn">
                                    <button type="button" disabled="disabled">決　定</button>
                                </div>
                            </div>
                        </div>
                    </form>
                    <div class="cover"></div>
                </div>
            </div><!-- /#conts -->
        </main><!-- /#main -->
        <footer id="footer">
            <p class="copyright"><small>厚生労働大臣許可　27-ユ-020100<br>&copy; QUICK CO.,LTD.</small></p>
            <div class="pMark"><a href="https://privacymark.jp/" rel="nofollow" target="_blank"><img src="/img/common/17001920_75_JP.png" alt="たいせつにしますプライバシー 17001920"></a></div>
        </footer>
    </div>
    <div class="cvTagBox">
        <script>
        var pageData = {
            "hashedEmail": ""
        };
        </script>

        <script type="text/javascript">
        (function() {
            var tagjs = document.createElement("script");
            var s = document.getElementsByTagName("script")[0];
            tagjs.async = true;
            tagjs.src = "//s.yjtag.jp/tag.js#site=h0B3pon&referrer=" + encodeURIComponent(document.location.href) + "";
            s.parentNode.insertBefore(tagjs, s);
        }());
        </script>
        <noscript>
            <iframe src="//b.yjtag.jp/iframe?c=h0B3pon" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </noscript>
    </div>
</body>

</html>