// 初期設定
var formParts = 'input, select, textarea',
	speed = 200;

var birthFlg = false; // 生年月日フラグ

var form_init = function(){
	var form = $('#form'),
		stepStatus = form.find('.stepStatus li'),
		stepArea = form.find('.stepArea'),
		panelArea = form.find('.panelArea');

	// stepStatus
	stepStatus.eq(0).addClass('active');

	// stepArea
	stepArea.hide().eq(0).show();
	// debug時初期ステップ指定用
	// stepArea.eq(0).removeClass('active').addClass('before');
	// stepArea.eq(1).show();
	// stepArea.eq(0).hide();
	// stepArea.eq(1).addClass('active');

	// tabindex
	stepArea.find('.input').find(formParts).attr('tabindex', -1);
	stepArea.eq(0).find('.input').find(formParts).attr('tabindex', 0);

	// panelArea
	panelArea.find('.agree').hide();
	panelArea.find('.prev').hide();
	panelArea.find('.send').hide();

	panelArea.find('.next').prop('disabled', true);
}

// stepの移動
var step_move = function(btn){
	var form = $('#form'),
		stepStatus = form.find('.stepStatus li');
		stepArea = form.find('.stepArea'),
		panelArea = form.find('.panelArea'),
		step = $('.stepArea.active').attr('data-step'),
		activeStep = 0

	switch(step){
		case '1':
			stepStatus.eq(0).removeClass('active').addClass('finished');
			stepStatus.eq(1).addClass('active');
			stepArea.eq(0).removeClass('active').addClass('before');
			stepArea.eq(1).show();
			setTimeout(function(){
				stepArea.eq(0).hide();
				stepArea.eq(1).addClass('active');

				panelArea.find('.prev').show();
			}, speed);

			activeStep = 1;
			break;

		case '2':
			if(btn.hasClass('prev')){
				stepStatus.eq(0).removeClass('finished').addClass('active');
				stepStatus.eq(1).removeClass('active');
				stepArea.eq(0).show();
				stepArea.eq(1).removeClass('active');
				setTimeout(function(){
					stepArea.eq(0).addClass('active').removeClass('before');
					stepArea.eq(1).hide();
					panelArea.find('.prev').hide();
				}, speed);

				activeStep = 0;
			}else{
				stepStatus.eq(1).removeClass('active').addClass('finished');
				stepStatus.eq(2).addClass('active');
				stepArea.eq(1).removeClass('active').addClass('before');
				stepArea.eq(2).show();
				setTimeout(function(){
					stepArea.eq(1).hide();
					stepArea.eq(2).addClass('active');
					panelArea.find('.agree').show();
					panelArea.find('.next').hide();
					panelArea.find('.send').show();
				}, speed);

				activeStep = 2;
			}
			birthFlg = false;
			break;

		case '3':
			if(btn.hasClass('prev')){
				stepStatus.eq(1).removeClass('finished').addClass('active');
				stepStatus.eq(2).removeClass('active');
				stepArea.eq(1).show();
				stepArea.eq(2).removeClass('active');
				setTimeout(function(){
					stepArea.eq(1).addClass('active').removeClass('before');
					stepArea.eq(2).hide();
					panelArea.find('.agree').hide();
					panelArea.find('.next').show();
					panelArea.find('.send').hide();
				}, speed);

				activeStep = 1;
			}else{
				stepStatus.eq(2).removeClass('active').addClass('finished');
				stepStatus.eq(3).addClass('active');
				stepArea.eq(2).removeClass('active').addClass('before');
				stepArea.eq(3).show();
				setTimeout(function(){
					stepArea.eq(2).hide();
					stepArea.eq(3).addClass('active');
				}, speed);

				activeStep = 3;
			}
			break;

	}

	// tabindex
	stepArea.find('.input').find(formParts).attr('tabindex', -1);
	stepArea.eq(activeStep).find('.input').find(formParts).attr('tabindex', 0);
	// ボタンの判定
	input_check(stepArea.eq(activeStep).find('input, select').eq(0), true);

}

// modalの選択処理
var modal_set = function(btn){
	var modal = btn.parents('.modalArea'),
		type = modal.attr('id');
	var form = $('#form'),
		stepArea = form.find('.stepArea');

	switch(type){
		// お住まい
		case 'prefModal':
			var setVal = modal.find('input:checked').next().text(),
				target = $('.selectPref');

			if(!setVal){
				setVal = '選択する'
			}
			target.val(setVal).change();

			break;

		// 希望勤務地
		case 'hopeModal':
			var setVal = modal.find('input:checked').map(function(i){
				return $(this).next().text();
			}).get().join('｜');

			var target = $('.hopePref').next('.viewTxt');
			if(setVal.length > 18){
				setVal = setVal.substr(0,(18)) + '…';
			}
			target.val(setVal).change();

			break;
	}

}

// modalの閉じる処理
var modal_close = function(btn){
	var modal = btn.parents('.modalArea'),
		type = modal.attr('id');

	switch(type){
		// お住まい
		case 'prefModal':
			break;

		// 希望勤務地
		case 'hopeModal':
			var target = $('.hopePref').next('.viewTxt');
			target.change();

			break;
	}
}

// エラーチェック
var input_check = function(self, skip){
	var area = self.parents('.stepArea'),
		target = self.parents('.input'),
		check = target.attr('data-check'),
		input = area.find('.input'),
		inputFlg = false,
		sendBtn = $('.panelArea').find('.send');

	// 現在選択した項目の前までのエラーチェック（選択中以降の項目は判定しない）
	for(var i = 0; i <= target.index(); i++) {
		// skipがtrueならエラーチェックは行わない
		if(skip == true){
			break;
		}

		switch(check){
			// ご経験 Step1
			case 'require_select':
				var val = input.eq(i).find('input, select').val(),
					box = input.eq(i).find('dd'),
					subTarget = self.parents('.license').find('.checkList li').length,
					subInput = area.find('.license').find('.checkList li');

				if(!val || val == '選択する'){
					if(!box.hasClass('error')){
						box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未選択です<p>')
					}
				}else{
					// 入力OKであれば'pass'というclassを付ける
					box.removeClass('error').addClass('pass').find('.errorTxt').remove();
					box.find('.selectModal').text("選択済み");
				}

				for(var j = 0; j < subTarget-1; j++) {
					var subVal = subInput.eq(j).find('input'),
						subBox = subInput.parents('dd');
					// エラーメッセージ削除
					subBox.removeClass('error').find('p').remove();
					if(!subVal.prop('checked')){
						if(!subBox.hasClass('error')){
							subBox.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未選択です<p>');
						}
					}else{
						// 入力OKであれば'pass'というclassを付ける
						subBox.removeClass('error').addClass('pass').find('p').remove();
						break;
					}
				}

				break;

			// プロフィール Step2
			case 'require_profile':
				var set = input.eq(i).find('input, select'),
					box = input.eq(i).find('dd'),
					setFlg = false;
				for(var j = 0; j < set.length; j++) {
					var val = set.eq(j).val();
					setFlg = false;

					// エラーメッセージ削除
					box.removeClass('error').find('p').remove();

					// 一つでも選択されてない項目があればエラー
					if(!val){
						if(!box.hasClass('error')){
							if(set.prop('tagName') == 'SELECT'){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未選択です<p>')
							}else{
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未入力です<p>')
							}
						}

						// エラー
						setFlg = true;
						break;
					}else{
						if(set.prop('name') == '_birth_y' || set.prop('name') == '_birth_m' || set.prop('name') == '_birth_d'){
							// 生年月日判定
							birth_y = input.eq(i).find('select[name="_birth_y"]').val(),
							birth_m = input.eq(i).find('select[name="_birth_m"]').val(),
							birth_d = input.eq(i).find('select[name="_birth_d"]').val(),
							birth = new Date(birth_y, birth_m-1, birth_d);

							// 生年月日の存在チェック
							if(!(birth.getFullYear()==birth_y && birth.getMonth()==birth_m-1 && birth.getDate()==birth_d)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】生年月日が正しくありません。<p>')
								// エラー
								setFlg = true;
								break;
							}

						}else if(set.prop('name') == 'surname_ruby' || set.prop('name') == 'firstname_ruby'){
							// カナ判定
							if(!val.match(/^[\u30a0-\u30ff\u3040-\u309f]+$/)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】ひらがなまたはカタカナでご入力ください。<p>')
								// エラー
								setFlg = true;
								break;
							}
						}
					}
				}
				if(!setFlg){
					// 入力OKであれば'pass'というclassを付ける
					box.addClass('pass');
				}

				break;

			// プロフィール Step3
			case 'require_profile_contact':
				var set = input.eq(i).find('input, select'),
					box = input.eq(i).find('dd'),
					setFlg = false;

				for(var j = 0; j < set.length; j++) {
					var val = set.eq(j).val();
					setFlg = false;

					// エラーメッセージ削除
					box.removeClass('error').find('p').remove();

					// 一つでも選択されてない項目があればエラー
					if(!val){
						if(!box.hasClass('error')){
							if(set.prop('tagName') == 'SELECT'){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未選択です<p>')
							}else{
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】未入力です<p>')
							}
						}

						// エラー
						setFlg = true;
						break;
					}else{
						if(set.prop('name') == 'email'){
							// email判定
							if(!val.match(/.+@.+\..+/)){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">【注】メールアドレスが正しくありません<p>')
								// エラー
								setFlg = true;
								break;
							}
						}else if(set.prop('name') == 'tel2'){
							// tel判定
							// ハイフンが入っていたら強制的に削除
							val = val.replace(/[━.*‐.*―.*－.*\?.*ー.*\-]/gi,'');
							// 全角 ⇒ 半角
							val = val.replace(/[Ａ-Ｚａ-ｚ０-９]/g,function(s){return String.fromCharCode(s.charCodeAt(0)-0xFEE0)});
							set.eq(j).val(val);

							var errTxt = '';
							if(val.match(/^\d{11}$/)){
								if(val.match(/^0[5|7|8|9]0/)){
									errTxt = '';
								}else{
									errTxt = '【注】電話番号の形式が不正です';
								}
							}else if(val.match(/^\d{10}$/)){
								if (val.match(/^0/)) {
									if (val.match(/^0[5|7|8|9]0/)) {
										errTxt = '【注】桁数が少ないです';
									} else {
										errTxt = '';
									}
								} else {
									errTxt = '【注】電話番号の形式が不正です';
								}
							}else{
								if(val.match(/\d{12,}$/)){
									errTxt = '【注】桁数が多いです';
								}else if(val.match(/\d{1,9}$/)){
									errTxt = '【注】桁数が少ないです';
								}else{
									errTxt = '【注】電話番号は半角数字で入力してください';
								}
							}

							if(errTxt){
								box.removeClass('pass').addClass('error').prepend('<p class="errorTxt">'+errTxt+'<p>')
								// エラー
								setFlg = true;
								break;
							}
						}
					}
				}
				if(!setFlg){
					// 入力OKであれば'pass'というclassを付ける
					box.addClass('pass');
				}

				break;
		}

		// skipにexが入っていれば、選択中の項目は判定しない
		if(skip == 'ex' && i == target.index() - 1){
			break;
		}
	}

	// passのclass判定（これは項目全て）
	for(var i = 0; i < input.length; i++) {
		if(check == 'require_experience'){
			var box = input.eq(i).find('.inputWrapper');
		}else{
			var box = input.eq(i).find('dd');
		}
		if(!box.hasClass('pass')){
			// passが付いていない項目があればエラーとする
			inputFlg = true;
			break;
		}
	}

	// エラーがなければボタン活性化
	if(sendBtn.is(':visible') && sendBtn.is(':disabled')) {
		setTimeout(function(){
			$('.panelArea').find('.next, .send').prop('disabled', inputFlg);
		}, speed);
	}else{
		$('.panelArea').find('.next, .send').prop('disabled', inputFlg);
	}
}

$(function(){
	// form init
	form_init();

	// btnの処理
	$('.btnArea').find('button').on('click', function(){
		if($(this).hasClass('send')){
			// form_submit();
		}else{
			step_move($(this));
			errorActive = undefined;
		}
	});

	// モーダル表示
	$('.selectModal').on('click', function(){
		var self = $(this);

		if(self.hasClass('selectPref')){
			$('#prefModal').addClass('active').css({'z-index':100});
		}else if(self.hasClass('hopePref')){
			$('#hopeModal').addClass('active').css({'z-index':100});
		}
	});

// modal control Start //
	var modal = $('.modalArea'),
		modalInner = modal.find('.inner'),
		modalInput = modal.find('input'),
		modalBtn = modal.find('.btn'),
		modalClose = modal.find('.closeIcon');

	// モーダル 決定
	modalBtn.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// モーダルの選択肢選択処理
		modal_set($(this));

		return false;
	});

	// モーダル 閉じる
	modalClose.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// モーダルの閉じる処理
		modal_close($(this));
	});
	modal.on('click', function(){
		modal.removeClass('active').animate({'z-index': -1},500);

		// モーダルの閉じる処理
		modal_close($(this).find('.closeIcon'));
	});
	modalInner.on('click', function(event){
		event.stopPropagation();
	});

	// モーダル内のバリデーションチェック
	modalInput.on('change blur', function(){
		var self = $(this).parents('.modalArea');
		var setVal = self.find('input:checked').map(function(i){
			return $(this).next().text();
		}).get().join();

		if(setVal){
			self.find('.btn').prop('disabled', false);
		}else{
			self.find('.btn').prop('disabled', true);
		}
	});

// modal control End //

// お持ちの資格 Start //
	var lic = $('.license .checkList li input'),
		lic_other = $('.license_other');

	lic.on('change', function(){
		// その他が選択された場合
		if($(this).val() == 6){
			if($(this).prop('checked')) {
				lic_other.show();
			}else{
				lic_other.hide();
			}
		}
	});
// お持ちの資格 End //

// これまでのご経験 Start //
	var exp = $('.experience'),
		direct = exp.find('#directForm'),
		fileup = exp.find('#fireupForm'),
		fileInput = $('#career_file'),
		textarea = $('#directTxt'),
		txtCount = direct.find('.textCount .num');

	// 入力項目表示
	exp.find('input').on('change', function(){
		if($('input[name="history"]:checked').val() == 1){
			direct.hide();
			fileup.show();
		}else{
			fileup.hide();
			direct.show();
		}
	});

	// 職務経歴書を添付する
	fileInput.change(function() {
		fileup.find('.txt').html($(this).val().replace("C:\\fakepath\\", ""));
	});

	fileup.find('.btn').on('click', function(){
		fileInput.click();
		if(fileup.hasClass('first')){
			setTimeout(function(){
				fileup.removeClass('first');
				fileup.prepend('<p class="errorTxt">【注】未選択です<p>');
			}, 200);
		}
	});
// これまでのご経歴 End //

// エラーチェック Start //
	var stepArea = $('.stepArea'),
		stepInput = stepArea.find('.input');
	var errorActive; // エラー判定中の項目

	stepArea.find('.input').find(formParts).on('focus change blur', function(e){
		var target = $(this).parents('.input'),
			self = $(this);

		// selectの場合は親要素のラベルで判定
		if(e.target.tagName == 'SELECT'){
			self = self.parents('.selectLabel');
		}

		// focus時の処理
		if(e.type == 'focus'){
			// 生年月日のどれかをfocusしたときにフラグを立てる
			if($(e.target).attr('name') == '_birth_y' || 
			$(e.target).attr('name') == '_birth_m' || 
			$(e.target).attr('name') == '_birth_d'){
				birthFlg = true;
			}else{
				birthFlg = false;
			}

			// 一番最初
			if(errorActive == undefined){
				errorActive = target.index();
			}else if(errorActive != target.index()){
				input_check($(this), 'ex');
				errorActive = target.index();
			}
		}else{
			// 判定する項目が2件以上あったら
			if(target.find('input, select').length > 1 && !target.hasClass('experience')){
				if(errorActive != target.index()){
					input_check($(this));
					errorActive = target.index();
				}else if(self.index() == target.find('input, select').length - 1){
					input_check($(this));
				}else if(self.index() == 3 || self.index() == 2){
					input_check($(this));
				}else if(self.attr('type') == 'checkbox'){
					input_check($(this));
				}
			}else{
				input_check($(this));
			}
		}
	});

	var form = $('#form');

	form.on('click', function(e){
		if(e.target.tagName != 'SELECT' &&
		 	e.target.tagName != 'INPUT' &&
		  	e.target.tagName != 'OPTION' &&
		  	e.target.tagName != 'BUTTON' &&
		   	e.target.tagName != 'SPAN'){
			if(birthFlg){
				input_check($('select[name="_birth_d"]'));
			}
		}
	});

// エラーチェック End //

	// submit後に完了ボタンと戻るボタンの押下を不可にして再押下させない
	$('#form').submit(function(){
		$('.btnArea  :button').prop('disabled', true);
		return true;
	});


// debug用
	var debug = false;

	if(debug){
		$('.panelArea').append('<button style="position:fixed; left:135px; top:210px;" class="dummy1">step1</button>');
		$('.panelArea').append('<button style="position:fixed; left:135px; top:240px;" class="dummy2">step2</button>');
		$('.panelArea').append('<button style="position:fixed; left:135px; top:270px;" class="dummy3">step3</button>');

		$('.dummy1').on('click', function(){
			// お住まい
			$('input[name="pr_id"]').eq(1).click();
			$('#prefModal').find('.btn').click();

			// 希望勤務地
			$('input[name="_hope_pref_id[]"]').eq(1).click();
			$('#hopeModal').find('.btn').click();

			return false;
		});


		$('.dummy2').on('click', function(){
			// 生年月日
			$('select[name="_birth_y"]').val('1990').change();
			$('select[name="_birth_m"]').val('10').change();
			$('select[name="_birth_d"]').val('10').change();

			// お名前
			$('input[name="surname"]').val('テストテスト').change();
			$('input[name="firstname"]').val('テスト').change();

			// おなまえ
			$('input[name="surname_ruby"]').val('てすとてすと').change();
			$('input[name="firstname_ruby"]').val('てすと').change();

			return false;
		});


		$('.dummy3').on('click', function(){
			// メールアドレス
			$('input[name="email"]').val('form.debug@gmail.com').change();

			// 電話番号
			$('input[name="tel2"]').val('08000000000').change();

			return false;
		});
	}

});