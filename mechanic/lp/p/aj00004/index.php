<?php 
function update($filepath) {
	echo date('YmdHis', filemtime($_SERVER['DOCUMENT_ROOT'].$filepath));
}
?>

<!DOCTYPE html>
<html lang="ja">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="自動車整備士の求人募集・転職支援なら、整備士ジョブズ。給料アップ、残業なしなど好条件の求人紹介・転職サービス。">
    <meta name="keywords" content="">
    <title>自動車整備士専門の転職サービス - 整備士ジョブズ</title>
    <link rel="icon" href="/img/common/favicon.ico" type="image/x-icon">
    <link rel="Shortcut Icon" type="image/x-icon" href="/img/common/favicon.ico">
    <!-- Start Visual Website Optimizer Synchronous Code -->
    <script>
    var _vis_opt_account_id = 279593;
    var _vis_opt_protocol = (('https:' == document.location.protocol) ? 'https://' : 'http://');
    document.write('<s' + 'cript src="' + _vis_opt_protocol +
        'dev.visualwebsiteoptimizer.com/deploy/js_visitor_settings.php?v=1&a=' + _vis_opt_account_id + '&url=' +
        encodeURIComponent(document.URL) + '&random=' + Math.random() + '">' + '<\/s' + 'cript>');
    </script>

    <script>
    if (typeof(_vis_opt_settings_loaded) == "boolean") {
        document.write('<s' + 'cript src="' + _vis_opt_protocol +
            'd5phz18u4wuww.cloudfront.net/vis_opt.js">' + '<\/s' + 'cript>');
    }
    /* if your site already has jQuery 1.4.2, replace vis_opt.js with vis_opt_no_jquery.js above */
    </script>

    <script>
    if (typeof(_vis_opt_settings_loaded) == "boolean" && typeof(_vis_opt_top_initialize) == "function") {
        _vis_opt_top_initialize();
        vwo_$(document).ready(function() {
            _vis_opt_bottom_initialize();
        });
    }
    </script>
    <!-- End Visual Website Optimizer Synchronous Code -->
    <link href="/mechanic/lp/p/css/common.css?<?php update('/mechanic/lp/p/css/common.css'); ?>" rel="stylesheet">
    <link href="/mechanic/lp/p/aj00004/css/index.css?<?php update('/mechanic/lp/p/aj00004/css/index.css'); ?>" rel="stylesheet">
    <link href="/mechanic/lp/p/css/multi_step.css?<?php update('/mechanic/lp/p/css/multi_step.css'); ?>" rel="stylesheet">
    <script src="/mechanic/lp/p/js/common.js?<?php update('/mechanic/lp/p/js/common.js'); ?>"></script>
    <script src="/js/entry.js?<?php update('/js/entry.js'); ?>"></script>
</head>

<body id="aj00004">
    <!-- Google Tag Manager -->
    <noscript>
        <iframe src="//www.googletagmanager.com/ns.html?id=GTM-PD7XKZ" height="0" width="0" style="display:none;visibility:hidden"></iframe>
    </noscript>
    <script>
    (function(w, d, s, l, i) {
        w[l] = w[l] || [];
        w[l].push({
            'gtm.start': new Date().getTime(),
            event: 'gtm.js'
        });
        var f = d.getElementsByTagName(s)[0],
            j = d.createElement(s),
            dl = l != 'dataLayer' ? '&l=' + l : '';
        j.async = true;
        j.src = '//www.googletagmanager.com/gtm.js?id=' + i + dl;
        f.parentNode.insertBefore(j, f);
    })(window, document, 'script', 'dataLayer', 'GTM-PD7XKZ');
    </script>
    <!-- End Google Tag Manager -->
    <div id="container">
        <header id="header">
            <div class="inner">
                <h1 class="logo"><img src="/mechanic/img/common/logo.svg" width="170" alt="整備士JOBS"></h1>
                <!-- <h1 class="logo"><img src="/mechanic/img/common/logo.svg" width="217" height="28" alt="整備士JOBS"></h1> -->
            </div>
        </header>
        <main id="main">
            <h2 class="mainHdg">
                <img src="/mechanic/lp/p/aj00004/img/mv_hdg01.png?<?php update('/mechanic/lp/p/aj00004/img/mv_hdg01.png'); ?>" alt="整備士専門の転職サービス">
            </h2>
            <div id="mainVisual">
                <div class="mainImg">
                    <img src="/mechanic/lp/p/aj00004/img/mv_hdg02.png?<?php update('/mechanic/lp/p/aj00004/img/mv_hdg02.png'); ?>" alt="今なら大幅な給料UPも。好条件の転職を実現。">
                    <p class="lead"><img src="/mechanic/lp/p/aj00004/img/mv_txt01.png?<?php update('/mechanic/lp/p/aj00004/img/mv_txt01.png'); ?>" alt="給料が低い、残業が多い、残業代が出ない、もっと休みが欲しい…今なら高給与求人も多く、待遇UPしやすくなっています。"></p>
                </div>
                <p class="offerHdg"><img src="/mechanic/lp/p/aj00004/img/mv_hdg03.png?<?php update('/mechanic/lp/p/aj00004/img/mv_hdg03.png'); ?>" alt="自動車整備士  求人例（一例）"></p>
                <div class="jobOffer">
                    <dl class="jobs">
                        <dt class="age">20代後半</dt>
                        <dd class="inner">
                            <p class="salary">年収<span class="num">530</span>万円</p>
                            <ul class="benefit">
                                <li>年間休日125日</li>
                                <li>マイカー通勤OK</li>
                            </ul>
                            <span class="dealer">大手国産ディーラー</span>
                        </dd>
                    </dl>
                    <dl class="jobs">
                        <dt class="age">30代後半</dt>
                        <dd class="inner">
                            <p class="salary">年収<span class="num">600</span>万円</p>
                            <ul class="benefit">
                                <li>残業なし</li>
                                <li>日祝休み</li>
                            </ul>
                            <span class="dealer">輸入車ディーラー</span>
                        </dd>
                    </dl>
                    <dl class="jobs">
                        <dt class="age">50代前半</dt>
                        <dd class="inner">
                            <p class="salary">年収<span class="num">750</span>万円</p>
                            <ul class="benefit">
                                <li>残業代全額支給</li>
                                <li>家族手当あり</li>
                            </ul>
                            <span class="dealer">大手輸入車ディーラー</span>
                        </dd>
                    </dl>
                </div>
                <a href="#formBox" class="anchBn subBtn">
                    <div class="free">
                        <p class="en">FREE</p>
                        <p class="ja">無料</p>
                    </div>
                    <div class="support">
                        <span class="icon">転職サポートを依頼する</span>
                    </div>
                </a>
            </div>
            <div id="conts">
                <section id="section01">
                    <h2 class="hdg02">
                        <span class="lead">年収UP・残業削減・休日増…</span>
                        <span class="companyName">整備士JOBS</span>を使うメリットは？
                    </h2>
                    <section id="section0101" class="flowBox">
                        <div class="inner">
                            <div class="txtBox">
                                <h3 class="hdg"><span class="num">1.</span>高給与・好待遇の求人が豊富</h3>
                                <p class="lead">
                                    <span>年収600万以上、残業なし、年間休日125日、日祝休み</span>など、<br>
                                    自動車整備士の高給与・好待遇求人が豊富。
                                </p>
                            </div>
                            <div class="imgBox">
                                <img src="/mechanic/lp/p/aj00004/img/sec01_img01.jpg?<?php update('/mechanic/lp/p/aj00004/img/sec01_img01.jpg'); ?>" alt="">
                            </div>
                        </div>
                    </section><!-- /#section0101 -->
                    <section id="section0102" class="flowBox">
                        <div class="inner">
                            <div class="txtBox">
                                <h3 class="hdg"><span class="num">2.</span>代行サービス</h3>
                                <p class="lead">
                                    在職中で時間がない方も安心。<br>
                                    応募や面談日程の調整など、<span>応募企業とのやりとりを代行</span>。
                                </p>
                            </div>
                            <div class="imgBox">
                                <img src="/mechanic/lp/p/aj00004/img/sec01_img02.jpg?<?php update('/mechanic/lp/p/aj00004/img/sec01_img02.jpg'); ?>" alt="">
                                <p class="etc">※一部企業を除きます。</p>
                            </div>
                        </div>
                    </section><!-- /#section0102 -->
                    <section id="section0103" class="flowBox">
                        <div class="inner">
                            <div class="txtBox">
                                <h3 class="hdg"><span class="num">3.</span>年収交渉・待遇交渉で条件UP</h3>
                                <p class="lead">
                                    より良い条件でのご入社を実現。<br>
                                    年収や手当、勤務店舗など、<span>好条件で入社</span>できるように交渉。
                                </p>
                            </div>
                            <div class="imgBox">
                                <img src="/mechanic/lp/p/aj00004/img/sec01_img03.jpg?<?php update('/mechanic/lp/p/aj00004/img/sec01_img03.jpg'); ?>" alt="">
                            </div>
                        </div>
                    </section><!-- /#section0103 -->
                    <div class="catch">
                        <span class="small">他にも、応募書類の作成・面談対策など・・・安心のサポートが多数。</span>
                        在職中で時間がない方も、希望通りの転職が実現
                    </div>
                </section><!-- /#section01 -->
                <div id="formCv">
                    <p class="balloon"><img src="/mechanic/lp/p/img/formcv_icon01.png" alt="所要時間1分"></p>
                    <div class="txtBox">
                        <em class="hdg">ご利用は無料。下記を入力するだけ</em>
                        <p class="lead">
                            求人探しや面接日程調整なども無料で代行。すきま時間で転職が進められます。<br>
                            仕事が忙しい・平日は動けない…という方もお気軽にお申し込み下さい。
                        </p>
                    </div>
                </div><!-- /#formCv -->
                <section class="formBox" id="formBox">
                    <h2 class="formHdg01">
                        <img src="/mechanic/img/common/logo.svg" width="217" height="28" alt="整備士JOBS">
                        <!-- <img src="/mechanic/img/common/logo.svg" width="217" height="28" alt="整備士JOBS"> -->
                        無料転職サポート お申込フォーム
                    </h2>
                    <script src="/mechanic/lp/p/js/multi_step.js"></script>
                    <script src="/mechanic/entry/js/ga.clientid.getset.js"></script>
                    <form action="#entry_form" id="form" name="entry_form" method="post" enctype="multipart/form-data">
                        <fieldset>
                            <input type="hidden" name="_exec" value="regist">
                            <input type="hidden" name="_form_mode" value="mechanic">
                            <input type="hidden" name="ga_client_id" id="ga_client_id">
                            <div class="section">
                                <ul class="stepStatus">
                                    <li>STEP1</li>
                                    <li>STEP2</li>
                                    <li>STEP3</li>
                                    <li>完了</li>
                                </ul>
                                <!-- step1 -->
                                <div class="stepArea active" data-step="1">
                                    <dl class="input" data-check="require_select">
                                        <dt>お住まい</dt>
                                        <dd>
                                            <label class="textLabel">
                                                <input type="text" name="_view_pref" value="選択する" class="selectModal selectPref" readonly>
                                            </label>
                                        </dd>
                                    </dl>
                                    <dl class="input license" data-check="require_select">
                                        <dt>お持ちの資格</dt>
                                        <dd>
                                            <ul class="checkList">
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="1"><span class="txt">三級自動車整備士</span>
                                                    </label></li>
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="2"><span class="txt">二級自動車整備士</span>
                                                    </label></li>
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="3"><span class="txt">一級自動車整備士</span>
                                                    </label></li>
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="4"><span class="txt">自動車検査員</span>
                                                    </label></li>
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="5"><span class="txt">資格なし</span>
                                                    </label></li>
                                                <li><label>
                                                        <input type="checkbox" name="_license_id[]" value="6"><span class="txt">その他</span>
                                                    </label></li>
                                                <li class="license_other"><label>
                                                        <textarea name="_sp_other_license" placeholder="その他の資格をお持ちの方はご入力ください"></textarea>
                                                    </label></li>
                                            </ul>
                                        </dd>
                                    </dl>
                                </div><!-- /step1 -->
                                <!-- step2 -->
                                <div class="stepArea profile" data-step="2">
                                    <dl class="input" data-check="require_profile">
                                        <dt>生年月日</dt>
                                        <dd>
                                            <label class="selectLabel">
                                                <select name="_birth_y">
                                                    <option value="">---</option>
                                                    <option value="1999">1999</option>
                                                    <option value="1998">1998</option>
                                                    <option value="1997">1997</option>
                                                    <option value="1996">1996</option>
                                                    <option value="1995">1995</option>
                                                    <option value="1994">1994</option>
                                                    <option value="1993">1993</option>
                                                    <option value="1992">1992</option>
                                                    <option value="1991">1991</option>
                                                    <option value="1990">1990</option>
                                                    <option value="1989">1989</option>
                                                    <option value="1988">1988</option>
                                                    <option value="1987">1987</option>
                                                    <option value="1986">1986</option>
                                                    <option value="1985">1985</option>
                                                    <option value="1984">1984</option>
                                                    <option value="1983">1983</option>
                                                    <option value="1982">1982</option>
                                                    <option value="1981">1981</option>
                                                    <option value="1980">1980</option>
                                                    <option value="1979">1979</option>
                                                    <option value="1978">1978</option>
                                                    <option value="1977">1977</option>
                                                    <option value="1976">1976</option>
                                                    <option value="1975">1975</option>
                                                    <option value="1974">1974</option>
                                                    <option value="1973">1973</option>
                                                    <option value="1972">1972</option>
                                                    <option value="1971">1971</option>
                                                    <option value="1970">1970</option>
                                                    <option value="1969">1969</option>
                                                    <option value="1968">1968</option>
                                                    <option value="1967">1967</option>
                                                    <option value="1966">1966</option>
                                                    <option value="1965">1965</option>
                                                    <option value="1964">1964</option>
                                                    <option value="1963">1963</option>
                                                    <option value="1962">1962</option>
                                                    <option value="1961">1961</option>
                                                    <option value="1960">1960</option>
                                                    <option value="1959">1959</option>
                                                    <option value="1958">1958</option>
                                                    <option value="1957">1957</option>
                                                    <option value="1956">1956</option>
                                                    <option value="1955">1955</option>
                                                    <option value="1954">1954</option>
                                                    <option value="1953">1953</option>
                                                </select>年
                                            </label>
                                            <label class="selectLabel">
                                                <select name="_birth_m">
                                                    <option value="">---</option>
                                                    <option label="1" value="1">1</option>
                                                    <option label="2" value="2">2</option>
                                                    <option label="3" value="3">3</option>
                                                    <option label="4" value="4">4</option>
                                                    <option label="5" value="5">5</option>
                                                    <option label="6" value="6">6</option>
                                                    <option label="7" value="7">7</option>
                                                    <option label="8" value="8">8</option>
                                                    <option label="9" value="9">9</option>
                                                    <option label="10" value="10">10</option>
                                                    <option label="11" value="11">11</option>
                                                    <option label="12" value="12">12</option>
                                                </select>月
                                            </label>
                                            <label class="selectLabel">
                                                <select name="_birth_d">
                                                    <option value="">---</option>
                                                    <option label="1" value="1">1</option>
                                                    <option label="2" value="2">2</option>
                                                    <option label="3" value="3">3</option>
                                                    <option label="4" value="4">4</option>
                                                    <option label="5" value="5">5</option>
                                                    <option label="6" value="6">6</option>
                                                    <option label="7" value="7">7</option>
                                                    <option label="8" value="8">8</option>
                                                    <option label="9" value="9">9</option>
                                                    <option label="10" value="10">10</option>
                                                    <option label="11" value="11">11</option>
                                                    <option label="12" value="12">12</option>
                                                    <option label="13" value="13">13</option>
                                                    <option label="14" value="14">14</option>
                                                    <option label="15" value="15">15</option>
                                                    <option label="16" value="16">16</option>
                                                    <option label="17" value="17">17</option>
                                                    <option label="18" value="18">18</option>
                                                    <option label="19" value="19">19</option>
                                                    <option label="20" value="20">20</option>
                                                    <option label="21" value="21">21</option>
                                                    <option label="22" value="22">22</option>
                                                    <option label="23" value="23">23</option>
                                                    <option label="24" value="24">24</option>
                                                    <option label="25" value="25">25</option>
                                                    <option label="26" value="26">26</option>
                                                    <option label="27" value="27">27</option>
                                                    <option label="28" value="28">28</option>
                                                    <option label="29" value="29">29</option>
                                                    <option label="30" value="30">30</option>
                                                    <option label="31" value="31">31</option>
                                                </select>日
                                            </label>
                                        </dd>
                                    </dl>
                                    <dl class="input" data-check="require_profile">
                                        <dt>お名前</dt>
                                        <dd>
                                            <input type="text" name="surname" value="" id="fNameSei" class="sei" placeholder="姓">
                                            <input type="text" name="firstname" value="" id="fNameMei" class="mei" placeholder="名">
                                        </dd>
                                    </dl>
                                    <dl class="input" data-check="require_profile">
                                        <dt>おなまえ</dt>
                                        <dd>
                                            <input type="text" name="surname_ruby" value="" id="fNameKanaSei" class="sei" placeholder="せい">
                                            <input type="text" name="firstname_ruby" value="" id="fNameKanaMei" class="mei" placeholder="めい">
                                        </dd>
                                    </dl>
                                </div> <!-- /step2 -->
                                <!-- step3 -->
                                <div class="stepArea profile" data-step="3">
                                    <dl class="input" data-check="require_profile_contact">
                                        <dt>メールアドレス</dt>
                                        <dd>
                                            <input type="email" name="email" id="email" value="" placeholder="例）xxx@yyy.zzz">
                                        </dd>
                                    </dl>
                                    <dl class="input" data-check="require_profile_contact">
                                        <dt>電話番号</dt>
                                        <dd>
                                            <input type="tel" name="tel2" id="tel2" value="" class="klimit-digit" placeholder="例）08000000000">
                                        </dd>
                                    </dl>
                                </div> <!-- /step3 -->
                                <div class="panelArea">
                                    <p class="agree">
                                        <a href="/ruleprivacy/" target="_blank">利用規約</a>に同意して
                                    </p>
                                    <div class="btnArea">
                                        <button type="button" value="戻 る" class="prev"><span>戻 る</span></button>
                                        <button type="button" value="次 へ" class="next"><span>次 へ</span></button>
                                        <button type="submit" value="完 了" class="send"><span>完 了</span></button>
                                    </div>
                                    <p class="rule">
                                        <a href="/ruleprivacy/" target="_blank">利用規約</a>
                                    </p>
                                    <a href="https://privacymark.jp/" rel="nofollow" target="_blank" class="pMark">
                                        <img src="/img/entry/17001920_75_JP.gif" alt="たいせつにしますプライバシー 17001920" width="75" height="75" />
                                    </a>
                                </div>
                            </div>
                            <div id="prefModal" class="modalArea">
                                <div class="inner">
                                    <h3 class="modalTtl">お住まいを選択してください</h3>
                                    <div class="scroll">
                                        <dl class="unit">
                                            <dt>北海道・東北</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="1"><span class="txt">北海道</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="2"><span class="txt">青森県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="3"><span class="txt">岩手県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="4"><span class="txt">宮城県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="5"><span class="txt">秋田県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="6"><span class="txt">山形県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="7"><span class="txt">福島県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>関東</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="8"><span class="txt">茨城県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="9"><span class="txt">栃木県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="10"><span class="txt">群馬県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="11"><span class="txt">埼玉県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="12"><span class="txt">千葉県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="13"><span class="txt">東京都</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="14"><span class="txt">神奈川県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>北陸・甲信越</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="15"><span class="txt">新潟県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="16"><span class="txt">富山県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="17"><span class="txt">石川県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="18"><span class="txt">福井県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="19"><span class="txt">山梨県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="20"><span class="txt">長野県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>東海</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="21"><span class="txt">岐阜県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="22"><span class="txt">静岡県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="23"><span class="txt">愛知県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="24"><span class="txt">三重県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>関西</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="25"><span class="txt">滋賀県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="26"><span class="txt">京都府</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="27"><span class="txt">大阪府</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="28"><span class="txt">兵庫県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="29"><span class="txt">奈良県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="30"><span class="txt">和歌山県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>中国・四国</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="31"><span class="txt">鳥取県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="32"><span class="txt">島根県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="33"><span class="txt">岡山県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="34"><span class="txt">広島県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="35"><span class="txt">山口県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="36"><span class="txt">徳島県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="37"><span class="txt">香川県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="38"><span class="txt">愛媛県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="39"><span class="txt">高知県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>九州・沖縄</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="40"><span class="txt">福岡県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="41"><span class="txt">佐賀県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="42"><span class="txt">長崎県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="43"><span class="txt">熊本県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="44"><span class="txt">大分県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="45"><span class="txt">宮崎県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="46"><span class="txt">鹿児島県</span>
                                                        </label></li>
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="47"><span class="txt">沖縄県</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                        <dl class="unit">
                                            <dt>海外</dt>
                                            <dd>
                                                <ul class="radioList">
                                                    <li><label>
                                                            <input type="radio" name="m_pref_id" value="48"><span class="txt">海外</span>
                                                        </label></li>
                                                </ul>
                                            </dd>
                                        </dl>
                                    </div>
                                    <button class="btn" disabled><span>決 定</span></button>
                                    <i class="closeIcon"></i>
                                </div>
                            </div>
                        </fieldset>
                    </form>
                </section>
            </div><!-- /#conts -->
        </main><!-- /#main -->

        <footer id="footer">
            <p class="copyright"><small>厚生労働大臣許可　27-ユ-020100　&copy; QUICK CO.,LTD.</small></p>
        </footer>
    </div>

    <div class="cvTagBox">
        <script>
        var pageData = {
            "hashedEmail": ""
        };
        </script>
        <!-- YTM -->
        <script type="text/javascript">
        (function() {
            var tagjs = document.createElement("script");
            var s = document.getElementsByTagName("script")[0];
            tagjs.async = true;
            tagjs.src = "//s.yjtag.jp/tag.js#site=SNbF7jO&referrer=" + encodeURIComponent(document.location.href) + "";
            s.parentNode.insertBefore(tagjs, s);
        }());
        </script>
        <noscript>
            <iframe src="//b.yjtag.jp/iframe?c=SNbF7jO" width="1" height="1" frameborder="0" scrolling="no" marginheight="0" marginwidth="0"></iframe>
        </noscript>
        <!-- END YTM -->

    </div>
</body>

</html>